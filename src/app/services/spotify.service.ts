import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {


  constructor(private http: HttpClient) {
    console.log('Spotify service ready');
  }

  getQuery(query: string){
    const url = `https://api.spotify.com/v1/${query}`;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQA4nrsmPAUmzGqa0-X0rcdvdwg2iqaX_b6UbA6XcifJzZqic4yAdm15muOfSzksZO4hwsjzsjW8YdSpH6I'
    });
    return this.http.get(url, { headers });
  }


  getNewReleases(){
    return this.getQuery('browse/new-releases?limit=20')
      .pipe(map( data => data['albums'].items ));
  }

  searchArtist(termino:string){
    console.log(termino);
    return this.getQuery(`search?q=${ termino }&type=artist&limit=15`)
    .pipe(map( data => data['artists'].items ));
  }



  getArtist(id:string){
      return this.getQuery(`artists/${ id }`);
      // .pipe(map( data => data['artists'].items ));
  }

}
