import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent {

  artistas:any[] = [];
  loading:boolean;

  constructor(private spotify:SpotifyService) {

  }

  buscar(term:string){
    console.log(term);
    if(term == ""){
      this.artistas = [];
      return false;
    }
    this.loading = true;
    this.spotify.searchArtist(term)
      .subscribe((data:any) => {
        console.log(data);
        this.artistas = data;
        this.loading = false;
      });
  }

}
